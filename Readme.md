# Littlespoon

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [About](#about)
- [Requirements](#requirements)
- [Installation](#installation)
	- [Windows](#windows)
	- [Mac](#mac)
	- [Linux](#linux)
		- [Ubuntu and other Debian based systems](#ubuntu-and-other-debian-based-systems)
		- [Red Hat Linux](#red-hat-linux)
- [Getting Started](#getting-started)
	- [Configuring Littlespoon.](#configuring-littlespoon)
- [Usage](#usage)
	- [Profiles](#profiles)
		- [Adding a new profile](#adding-a-new-profile)
		- [Editing profiles](#editing-profiles)
		- [Deleting profiles](#deleting-profiles)
	- [Pipelines](#pipelines)
		- [Adding a new Pipeline](#adding-a-new-pipeline)
		- [Editing pipelines](#editing-pipelines)
		- [Deleting pipelines](#deleting-pipelines)
- [Exporting Data](#exporting-data)
- [Bugs and Support](#bugs-and-support)
- [Contributing](#contributing)

<!-- /TOC -->

## About

Littlespoon is a GUI front-end for [Littlefork](https://gitlab.com/littlefork/littlefork) - a command line tool for retrieving and transforming data for various sources by using a series of [plug-ins](https://gitlab.com/littlefork?utf8=%E2%9C%93&filter_projects=littlefork-plugin).

Users can
1. Set up queries by selecting plug-ins and providing information (e.g. search terms, hashtags, twitter names) required for each plug-in.
2. Run pipelines - a series of transformations for a query (e.g. search for youtube videos and publish them)


## Requirements
- A Linux, Windows or Mac computer
- Some knowledge of the terminal and the command line

-

## Installation
### Windows
- Download and run the `msi` installer.

### Mac
- Download and run `dmg` file.

### Linux
#### Ubuntu and other Debian based systems
- Download the `.deb` and in a terminal window run the following command
```
sudo dpkg -i <installer>.deb
```

#### Red Hat Linux
- Download the '.rpm` and in a terminal window run the following command

```
sudo yum install <installer>.rpm
```

## Getting Started
### Configuring Littlespoon.

## Usage
### Profiles
#### Adding a new profile
#### Editing profiles
#### Deleting profiles
### Pipelines
#### Adding a new Pipeline
#### Editing pipelines
#### Deleting pipelines
## Exporting Data
## Bugs and Support
## Contributing
