# Littlespoon technical plan

Littlefork, until now, is an application only excecutable, configurable, and usable from the terminal.  This document documents our plan to create a Graphical User Interface **littlespoon**

---------------------------------------------------------------

## About Littlefork, Littlespoon

Littlespoon will be a series of tools to run littlespoon in a graphical, cross platform environment.
Littlefork is a sequential data investigation pipeline.
Littlefork has three main functions and abilities.


### Littlefork Functions:
- _New Data_ : Fetching a subset of the data on the internet and storing it.  Repeatable searches.
- _Relationships_ : Finding relationships in linked assets from separate data units.
- _Revisions_ : Updating data units with new versions while retaining the old data.

Littlefor can be expressed as a function.

```
plugin === function
pipeline === series of plugins === function
LF = [queries] X [pipelines]
pipeline(query, config) -> Data
```

The pipeline in littlefork constructs an envelope for the data.  This is passed from plugin to plugin, and returned at the end.

```
{
	queries: [{query},{query}],
	data:	 [{unit},{unit}],
	stats:	 {}
}
```

#### Modes Of Operation
	- Headless - from a cron job or configured on a Server
	- Ad-hoc - directly from the terminal or UI

----------------------------------------------------------------

## About LittleSpoon

littlespoon will communicate throught the littlefork backend via an api, which can be in a virtualbox or on a server somewhere.  An individual user will be able to download the littlespoon package, after configuring a virtual box.

```
+-------littlespoon-------------------------+
|             Electron                      |
|                                           |
|   +-----------------------------------+   |
|   |          Web Interface            |   |         user's computer
|   |                                   |   |         +---------littlespoon--------+
|   |   +---------------------------+   |   |         |electron     virtual box    |
|   |   |         Api               |   |   |         | +-----+      +-------+     |
|   |   |   +-------------------+   |   |   |         | |ls   | api  |little |     |
|   |   |   |                   |   |   |   |         | |UI   +------+fork   |     |
|   |   |   |    Littlefork     |   |   |   |         | +-----+      +-------+     |
|   |   |   |   +----------+    |   |   |   |         +----------------------------+
|   |   |   |   | plugins  |    |   |   |   |              |api
|   |   |   |   +----------+    |   |   |   |              |
|   |   |   +-------------------+   |   |   |          +---+-----+
|   |   +---------------------------+   |   |          |little   |
|   |                                   |   |          |fork     |
|   +-----------------------------------+   |          +---------+
|                                           |            Server
|                                           |
+-------------------------------------------+


```

### Dependencies
- virtualbox
- ui
	- electron

The Idea here is that the 'backend' can reliably run on a linux environment and be developed in ways which we are used to develoing.  Virtual box will then ensure that it will run on the largest set of computers and operating systems possible.


## UI Design

Our goal for the littlespoon-UI is to allow for a relatively non-technical user to be able to operate littlefork.  We are aiming to account for the most used functions of littlefork to be accomplished easily.  We are actively researching what current users of the software use it for and need.

The User should be able to iterate on the work that they have done, redo things, and easily construct new pipelines and queries.

### LS Functions

The LS-UI should accomplish 4 major functions.  These are not necessarily separate 'views' to the interface, but more of a what the entire interface together will need to accomplish.

- operation
- data view/filter/slice/dice
- collaboration (import/export)
- history/log

First we will concentrate on designing and building the OPERATION function of the UI.  Not only does this have to work before anything else is possible, but may already be useful with only it being finished.  In other words, it is a discrete chunk that we can start on immediately.

#### OPERATION design elements:

We have identified the following Designed elements as necessary for the user to operate littlefork successfully.

- Project CRUD
- Promt
- Run/pipeline editor
- Notebook/project view/playground
- query CRUD

### What the UI infers

Littlespoon-UI will work for most uses of littlefork, and will infer sensible defaults.

Small examples of these:
- a query is tied to a plugin, therefore when a QUERY is included in the RUN, the matching plugin of the QUERY will be added to the beginning of the PIPELINE
- by default, the last step of the pipeline will be a DB-SAVE.  this is undo-able

### Human Language

inside of the PROMPT, we are planning to use human language to make the point of littlefork clearer.

	get [query]
	do [pipeline]
	do [pipeline]

An exampe of this would be

	get "Youtube Aleppo"
	do "save videos and publish"

the "get" infers a query.  this is editable.

the "do" infers a pipeline.  this is also editable.


## Run Editor // Pipeline Constructer

- available plugins
	- descriptions of functionality
- pipeline constructor
- configuration options for plugins


## Bits and Pieces

### Query Data format:

```
{
	source: 'twitter_search', (plugin),
	terms: [terms]
}
```

### API


```
/projects/[project]/

	            /runs                        GET, POST
	            /runs/[id]                   GET, SOCKET
	            /runs/[id]/data              GET

	            /pipelines                   GET, POST
	            /pipelines/[id]              GET, PUT

	            /plugins                     GET

	            /queries                     GET, POST
	            /queries/[id]                GET, PUT
```


I could also imagine to use websockets for the API, or a mix and match.

Ideally the API should work a lot with the file system.  Maybe even use `gitjs` or similar
to reflect every interaction using git. Exporting a project than only involved
zipping up a directory.


#### LS Data models

LS-UI will store its configuration and use data models in the background.  These will be converted into littlefork commands when they need to be, and much can be inferred from them.

```

project
{
	name: "",
	desc: "",
	api_url: "", // local by default, remote possible
	config: {
		twitter_api_key: "3fdjin399sdf",
		// pipeline and query config get stored here, to prefill on reuse
	}
}

run
{
	uuid: "",
	// each run creates a unique id
	name: "",
	actions: [
		{query},
		{pipeline},
		{pipeline},
		// debate about whether to store w/ config data, or
		// a reference to a configured object
	],
	actionshash: "",
	status: "completed|failed|running",
	stats: {} // need to know what this can look like
}

pipeline
{
	name: "",
	description: "",
	plugins: ["","",""],
	config: {} // filled in in pipeline editor, saved to project 
			   // defaults
}

querylist
{
	name: "",
	desc: "",
	queries: [{},{}],
	config: {} // filled in in pipeline editor, saved to project 
			   // defaults
}

plugin
{
	name: "",
	description: "",
	config_args: [
			{
				label: "",
				optional: bool,
				default: "",
				type: "widget"
			}
		]
	query_args: {
		label: "",
		type: "widget",
	}
}
```

When a run is excecuted, configuration data will be merged `project -> run` , so that each individual run enables using specific configuration details.

Configuration data is stored in the project (defaults) and the run (specific instance)

Queries and Pipelines are reusable elements.

First Run
	- create a project
	- add a query
	- add a run + pipeline
	- configure the run
	- run

## Use cases:
- Syrian Archive
	- a group of researches working together to archive from a variety of sources
	- publishing of a dataset
- Independant researcher
	- ability to change terms and functionality often
	- exploratory element

## Changes that have to happen to the current LF

- Rename *profiles* to *queries*.
- *Profiles* are currently groups of *queries*. Turn this into a flat list of
  objects:

  ```
  [{
    query: 'twitter_search',
    term: '#gamergate'
  }, {
    query: 'instagram_feed',
    term: 'fruit__machine'
  }]
  ```

- Inject a *runId* into each unit. Maybe make this part of core, maybe
  implement as plugin.
- Extract the stats mechanism from core, probably turn it into a plugin?
- Disentangle the plugin runner from the command line tool.
- Each plugin should export any expected query key additional to config
  options.
- Probably some more ...

## Curated Littlefork

This is the list of plugins i would aim for as the first version of curated
Littlefork. Plugins marked with a star don't exist yet, probably all need some
sort of touching.

- twitter_feed
- twitter_search
- twitter_followers
- twitter_following
- instagram_feed
- youtube_channel
- youtube_download
- http_get (*)
- http_put (*)
- http_post (*)
- csv_import (*)
- csv_export
- duckduckgo_search
- searx_search
- search
- language_detection
- exif_extract
- mongodb_fetch
- mongodb_store
- mongodb_difference (*)
- mongodb_intersection (*)
- wayback-machine_memento
- mail
